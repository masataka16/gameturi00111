﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPieTemplate_HAVE : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

[System.Serializable]
public abstract class IPieTemplate : MonoBehaviour {

	[HideInInspector]
	public PieMenu ApplePie;
	[HideInInspector]
	public PieMenu ExtraPie;
	public abstract void Open();
	public abstract void Handle();


}
