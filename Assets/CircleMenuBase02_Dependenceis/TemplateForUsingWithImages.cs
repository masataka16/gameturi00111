﻿using UnityEngine;
using System.Collections;

public class TemplateForUsingWithImages : IPieTemplate{

	public int a;
//	public PieMenu ApplePie;
	public int b;
	//	public GUIContent[] Options;
	public ExecutableOption[] Options;

	public GUIContent[] SubOptions;
	public string ButtonToOpen = "Fire2";
	public IPieTemplate extraMenu;

	public float dist_x,dist_y =0; 


	public void Start () {
		
							//If the "ButtonToOpenWidth" button was pressed and released...
//		if (!ApplePie.Active) {ApplePie.InitPie(Options); Debug.Log("hh"); }					//...If not Initialized yet, Initialize a new pie menu option set using the Options array,
		//	public PieMenu InitPie (ExecutableOption[] options)に変えてみるとどうなるか
		//●●●●●ここで　引数が　ExecutableOption[] OptionsのInitPie(ExecutableOption[] Options) を選んで●●●ハードティング●●●して呼んでいる　↑にパブリックでエディターから値は得ている

		if (!ApplePie.Active) {ApplePie.InitPie ( Options); Debug.Log("hh"); }					//...If not Initialized yet, Initialize a new pie menu option set using the Options array,
		//	public PieMenu InitPie (ExecutableOption[] options)に変えてみるとどうなるか



		else if (ApplePie.SelectedExecutableOption == null) Close();	//If nothing is selected, close.

		else ApplePie.Execute();										//Execute the actual selected option. The method name is defined in the inspector.




		//		Handle();
	}

	public void Template01(){

		Debug.Log ("Template01()すなわちTemplateForUsingWithImagesの中のメソッド呼ばれた");

	}


	public void Update(){
		ApplePie.Center = new Vector2 (Screen.width / 2 + dist_x, Screen.height / 2 + dist_y ); //Align it to the center of the screen
	
	
		
    
	//We don't need the update now, because we give the control over this script to "TemplateForClickableAreas".
	
		if (Input.GetMouseButtonDown(0)) {					//If the "ButtonToOpenWidth" button was pressed and released...
		if (!ApplePie.Active) ApplePie.InitPie(Options); 					//...If not Initialized yet, Initialize a new pie menu option set using the Options array,
			else if (ApplePie.SelectedExecutableOption == null){ 
				Debug.Log ("ホーム画面に戻る動きをここに作成");
				   //Close();	//If nothing is selected, close.
		}
		else ApplePie.Execute();										//Execute the actual selected option. The method name is defined in the inspector.
    
	
    }

//		Handle();
	}

   



	public override void Open ()
	{
	}
		
/*
	public override void Open ()
	{
		if (!ApplePie.Active) ApplePie.InitPie(Options); 								//Initialize a new pie menu option set.
		else if (!(extraMenu != null && ExtraPie.Active)) {
			int selected_Command = ApplePie.Selected; 				//Returns the selected command's number and closes the menu.
			
			if (ApplePie.MenuSet == Options)
			{
				
				switch (selected_Command)
				{
				case 0: 																//The 0 is "Next" so we enter the SubOptions.
					if (extraMenu == null) ApplePie.TransitionPie(SubOptions);
					else {
						ApplePie.Freeze(true);											//Freezes down the Pie Menu, so you can't control it.
						ExtraPie = extraMenu.ApplePie.InitPie(SubOptions);
					}
					break;
				default: 																// The default is leave it closed.
					Close();
					break;
				}
			}
			else if (ApplePie.MenuSet == SubOptions)
			{
				
				switch (selected_Command)
				{
				case 0: 																//The 0 command is "Back" so we "reload" the original Options.
					ApplePie.TransitionPie(Options);
					break;
				default: 																// The default is leave it closed.
					break;
				}
			}
			if (!ApplePie.Frozen) Close();
		}
		else if (ExtraPie != null && extraMenu != null && ExtraPie.Active) {
			int sel = ExtraPie.Selected;
			if (sel == 0) {
				ExtraPie.TransitionPie(new string[] {"Reload", "Return","Quit"});
			}
			else if (sel == 1) {
				ExtraPie.Return();
				ApplePie.Freeze(false);
			}
			else {
				ExtraPie.Close();
				ApplePie.Freeze(false);
				Close();
			}
		}
	}

	*/

	public void Close () {
		ApplePie.Close();
	}

	public override void Handle (){
	}


//●●●ApplePie.Run()で円グラフを描いてる　ただしApplePie.Activeがtrueの時に描く 
	//●●●showThePie = trueの時に描くということ　ApplePie.Activeは、 void ApplePie.Active{ retrut showpie;} なのだ
	public  void OnGUI () {
	//	ApplePie.Center = new Vector2 (Screen.width/2,Screen.height/2); //Align it to the center of the screen
		ApplePie.Run(); //Run the main Pie Menu wrapper.
	}
}